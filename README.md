# hero_flutter

Quando um PageRoute é pressionado ou exibido com o Navegador, 
o conteúdo da tela inteira é substituído. Uma página antiga 
desaparece e uma nova página aparece. Se houver um recurso 
visual comum em ambas as páginas, pode ser útil orientar 
o usuário para que o recurso se mova fisicamente de 
uma página para outra durante a transição das páginas. 

Essa animação é chamada de animação de hero. 

Os widgets de hero "voam" na sobreposição do Navegador 
durante a transição e, enquanto estão em voo, por padrão, 
não são mostrados em seus locais originais das páginas 
antigas nas novas.

Este exemplo demosntra isso.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
